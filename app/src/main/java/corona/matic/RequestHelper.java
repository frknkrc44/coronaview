package corona.matic;

import android.os.*;
import java.io.*;
import java.net.*;

public class RequestHelper {
	
	public static abstract class OnResultListener {
		
		private void start(String apikey){
			new BackgroundTask().execute(apikey);
		}
		
		public abstract void onSuccess(String data);
		public abstract void onFail();
		
	}
	
	public static void doAsyncTask(OnResultListener resultListener, String apikey){
		listener = resultListener;
		listener.start(apikey);
	}
	
	private static OnResultListener listener;
	
	private static class BackgroundTask extends AsyncTask<String,Void,String> {

		@Override
		protected String doInBackground(String[] p1) {
			try {
				URL url = new URL("https://api.collectapi.com/corona/totalData");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.addRequestProperty("Authorization",p1[0]);
				conn.addRequestProperty("Content-Type","application/json");
				conn.addRequestProperty("User-Agent","okhttp/3.0");
				
				conn.setDoInput(true);
				conn.connect();
				
				InputStreamReader isr = new InputStreamReader(conn.getInputStream());
				char[] buf = new char[1024];
				int count;
				StringBuilder builder = new StringBuilder();
				
				while((count = isr.read(buf,0,buf.length)) > 0){
					builder.append(buf,0,count);
				}
				
				return builder.toString();
			} catch(Throwable t){}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if(result == null){
				listener.onFail();
			} else {
				listener.onSuccess(result);
			}
		}
		
	}
	
}
