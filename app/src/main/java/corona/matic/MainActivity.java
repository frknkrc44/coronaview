package corona.matic;

import android.app.*;
import android.os.*;
import android.widget.*;
import org.json.*;

public class MainActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		refresh();
    }
	
	private void refresh(){
		RequestHelper.OnResultListener listener = new RequestHelper.OnResultListener(){
			TextView text = findViewById(R.id.mainTextView);
			
			@Override
			public void onSuccess(String data){
				try {
					JSONObject obj = new JSONObject(data);
					if(isSuccess(obj)){
						String death = getStr(obj,"totalDeaths");
						String cases = getStr(obj,"totalCases");
						String recov = getStr(obj,"totalRecovered");
						text.setText(
							"💀: " + death +
							"\n🚑: " + cases +
							"\n💊: " + recov
						);
					} else {
						onFail();
					}
				} catch(Throwable t){}
			}

			@Override
			public void onFail(){
				text.setText("❗");
			}

		};

		RequestHelper.doAsyncTask(listener,"apikey your_key");
	}
	
	private boolean isSuccess(JSONObject obj) throws JSONException {
		return obj.has("success") && obj.getBoolean("success");
	}
	
	private String getStr(JSONObject obj, String key) throws JSONException {
		JSONObject o = obj.getJSONObject("result");
		if(o.has(key)){
			return o.getString(key);
		}
		
		return "";
	}
}
